import java.util.ArrayList;


public class Client {

	private String nomEntreprise;
	
	private String adresse;
	
	private int siret;
	
	private String ape;
	
	private ArrayList<maintenance>c_listemaintenance;
	
	private Devis lesdevis;
	
	public Client(String nomEntreprise,String adresse,int siret,String ape,Devis lesdevis){
		
		this.nomEntreprise=nomEntreprise;
		this.adresse=adresse;
		this.siret=siret;
		this.ape=ape;
		this.lesdevis=lesdevis;
		c_listemaintenance=new ArrayList<maintenance>();
	}
	
	public String getNomentreprise(){
		return nomEntreprise;
	}
	
	public String getAdresse(){
		return adresse;
	}
	
	public int getSiret(){
		return siret;
	}
	
	public String getApe(){
		return ape;
	}
	
	public void setAdresse(String adresse){
		this.adresse=adresse;
	}
	
	public void afficher(){
		System.out.println("Le nom de l'entreprise "+nomEntreprise+"Adresse "+adresse+"Siret "+siret+"APE"+ape+"Les devis"+lesdevis);
		if(c_listemaintenance.size()==0){System.out.println("Pas de maintenances");}
		else{for(int i=0;i<c_listemaintenance.size();i++){c_listemaintenance.get(i).afficher();}
	}
	
	
	
}
}