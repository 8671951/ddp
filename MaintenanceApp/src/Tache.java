
public class Tache {

	private String designation; 
	
	private float prixHT;
	
	private float prixTTC;
	
	private int quantite;
	
	public Tache(String designation,float prixHT,float prixTTC,int quantite){
		this.designation=designation;
		this.prixHT=prixHT;
		this.prixTTC=prixTTC;
		this.quantite=quantite;
	}
	
	public float getPrixHT(){
		return prixHT;
	}
	
	public float getPrixTTC(){
		return prixTTC;
	}
	
	public void afficher(){
		System.out.println("Designation "+designation+"PrixHT "+prixHT+"PrixTTC "+prixTTC+"Quantit� "+quantite);
	}
}
