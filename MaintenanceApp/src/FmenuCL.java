import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;
public class FmenuCL extends JFrame{
	/*Creation de la Classe graphique menu client
	 * validation du compte rendu et d'un ticket
	 * cr�ation du container, des labels, des zones de textes et des boutons
	 * Ensuite creation du constructeur
	 */
	Container contenu;
	
	JButton bouton1=new JButton("Validation compte rendu");
	JButton bouton2=new JButton("Validation d'un Ticket");
	JButton bouton=new JButton("Valider");
	JLabel L1= new JLabel("Menu client");
	public FmenuCL (){
		/*
		 * dimension de la fenetre
		 */
		this.setBounds(100, 100, 600, 400);
		this.setTitle("application de maintenance");
		this.setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		contenu=this.getContentPane();
		contenu.setLayout(null);
		/*
		 * ajout des boutons et des labels au container
		 */
		contenu.add(bouton1);
		contenu.add(bouton2);
		contenu.add(bouton);
		contenu.add(L1);
		/*
		 * dimensions des boutons et des labels
		 */
		L1.setBounds(245,5,150,50);
		bouton1.setBounds(130,50,300,50);
	
		bouton2.setBounds(130,120,300,50);
			
		bouton.setBounds(300,250,100,50);
		
		}
	}


