import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Classe d'accès aux données contenues dans la table article
 * 
 * @author aurele nassara
 * @version 1.2
 * */
public class newDAO {
	final static String URL = "jdbc:oracle:thin:@localhost:1521:xe";
	final static String LOGIN = "GMAO";  
	final static String PASS = "nassara18";
	
	/**
	 * Constructeur de la classe
	 * 
	 */
	
	public newDAO(){
			// chargement du pilote de bases de données
			try {
				Class.forName("oracle.jdbc.OracleDriver");
			} catch (ClassNotFoundException e) {
				System.err.println("Impossible de charger le pilote de BDD, ne pas oublier d'importer le fichier .jar dans le projet");
			}

	}

	public int ajouterdevis(Devis devis){ 
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("Insert into devis (devis_date, devis_liste, devis_numero) values(?,?,?)");
			/*ps.setString(1,devis.getDate());
			ps.setString(2,devis.getListetache());
			ps.setInt(3, devis.getNumero());*/
			
			//on ex�cute la requ�te
			retour =ps.executeUpdate();
			
			
			
			
					
				
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		
		
		return retour;
		}
	
	public int authentificationid(String id){
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs= null;
		int retour =0;
		//connexion � la base de donn�es
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT US_IDTF FROM UTILISATEUR WHERE US_IDTF = ?");
			System.out.println("verification de l'id1");
			ps.setString(1, id);
			
			//on ex�cute la requ�te
			rs= ps.executeQuery();
			System.out.println("verification de l'id2");
			System.out.println("rs "+ rs.next());
			while(rs.next())
			{
				System.out.println("verification de l'id3");
				if(id.equals(rs.getString("US_IDTF")))
					retour=1;
				else retour=0;
					
			}	
	
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		
		
		return retour;
	}
	
	public int authentificationmdp(String mdp){
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs= null;
		int retour =0;
		//connexion � la base de donn�es
		
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("SELECT US_MDP from UTILISATEUR where US_MDP = ?");
			ps.setString(1, mdp);
			
			//on ex�cute la requ�te
			rs= ps.executeQuery();
			
			//on parcourt les lignes du r�sultat
			
			while(rs.next())
				
				if(mdp.equals(rs.getString("US_MDP")))
					retour=1;
				else retour=0;
					
				
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		
		
		return retour;
	}
	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
				new Bienvenue();
				//new fenetre2();
				//new fenetre3();
				//new fenetre4();
				//new interface1();
				//new interface2();
	}

	public int ajouterticket(Ticket ticket1) {
		// TODO Auto-generated method stub
		
		Connection con = null;
		PreparedStatement ps = null;
		int retour = 0;
		try {
			con = DriverManager.getConnection(URL, LOGIN, PASS);
			ps = con.prepareStatement("Insert into ticket (ticket_nomclt, ticket_prenomclt, ticket_lieu, ticket_degre, ticket_description) values(?,?,?,?,?)");
			ps.setString(1,ticket1.getNomClient());
			ps.setString(2,ticket1.getPrenomClient());
			ps.setString(3,ticket1.getlieu());
			ps.setInt(4,ticket1.getDegreImportance());
			ps.setString(5,ticket1.getDescription());
			
			//on ex�cute la requ�te
			retour =ps.executeUpdate();
			
			
			
			
					
				
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			// fermeture du preparedStatement et de la connexion
			try {
				if (ps != null)
					ps.close();
			} catch (Exception ignore) {
			}
			try {
				if (con != null)
					con.close();
			} catch (Exception ignore) {
			}
		}
		
		
		return retour;
		}
	
	}

	

