CREATE TABLE client_clt(
clt_id number(2),
clt_nom varchar2(40) CONSTRAINT UN_cltnom UNIQUE,
clt_siret varchar2(14) CONSTRAINT UN_cltsiret UNIQUE,
clt_adr varchar2(100) CONSTRAINT UN_cltadr UNIQUE,
clt_ape varchar2(5) CONSTRAINT UN_cltape UNIQUE,

CONSTRAINT PK_clt PRIMARY KEY (clt_id) 
);

create table utilisateur_us(
us_idtf number (4),
us_mdp varchar2 (10),
CONSTRAINT PK_us PRIMARY KEY (us_idtf),
CONSTRAINT FK_us_clt FOREIGN KEY (us_clt_id) references client_clt(clt_id) on Delete Cascade);
insert into utilisateur_us values(111,'java');

create table devis(
devis_date date ,
devis_id number (2),
devis_clt_id number(2),
devis_liste varchar2 (20),
CONSTRAINT PK_devis PRIMARY KEY (devis_id),
CONSTRAINT FK_devis_clt_id FOREIGN KEY (devis_clt_id) REFERENCES client_clt(clt_id) /*on Delete Cascade*/
);

create table ticket(
ticket_nomclt varchar2(20),
ticket_prenomclt varchar2(20),
ticket_lieu varchar2(20),
ticket_us_idtf number(3),
ticket_degre number (3),
ticket_description varchar2(50),
ticket_id number (3),
CONSTRAINT PK_ticket PRIMARY KEY (ticket_id),
CONSTRAINT FK_ticket_us_idtf FOREIGN KEY (ticket_us_idtf) REFERENCES utilisateur_us(us_idtf)
);



CREATE TABLE compterendu_cpt(
cpt_description varchar2(100) ,
cpt_mtn_idoperateur number(2),
cpt_nomclient varchar2(10) ,
cpt_date date ,
cpt_idoperateur number(2) ,
CONSTRAINT PK_cpt PRIMARY KEY (cpt_idoperateur),
CONSTRAINT FK_cpt_mtn_idoperateur FOREIGN KEY (cpt_mtn_idoperateur) REFERENCES maintenance_mtn(mtn_id) 
);


CREATE TABLE type_maintenance_tmtn(
tmtn_id number(2),
tmtn_nom varchar(10) unique,

Constraint PK_tmtn PRIMARY KEY(tmtn_id) 
);


CREATE TABLE maintenance_mtn(
mtn_id number(2),
mtn_clt_id number(2),
mtn_titre varchar2(10) CONSTRAINT NN_mtntitre NOT NULL,
mtn_info varchar(50) CONSTRAINT NN_mtninfo NOT NULL,
mtn_tmtn_id number(2),
mtn_date_debut date,

CONSTRAINT PK_mtn PRIMARY KEY(mtn_id) ,
CONSTRAINT FK_mtnclt FOREIGN KEY (mtn_clt_id) REFERENCES client_clt(clt_id) on Delete Cascade,
CONSTRAINT FK_mtn_tmtn FOREIGN KEY (mtn_tmtn_id) REFERENCES type_maintenance_tmtn(tmtn_id) on Delete Cascade
);